﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class makeSureText : MonoBehaviour
{
    public Text text;

    // Start is called before the first frame update
    void Start()
    {
        //取得したTextをピッタリ収まるようにサイズ変更(Heightが長い状態)
        text.rectTransform.sizeDelta = new Vector2(text.preferredWidth, text.preferredHeight);

        //再度、ピッタリ収まるようにサイズ変更(Heightもピッタリ合うように)
        text.rectTransform.sizeDelta = new Vector2(text.preferredWidth, text.preferredHeight);        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
